# Get luca locations from OSM.
# We use overpass, as this also delivers
# results as GeoJSON.
#
# Other tools offer OSM JSON or OSM XML which then has to be converted to
# GeoJSON.
#
# See https://github.com/mvexel/overpass-api-python-wrapper for details.


import overpass
import json

api = overpass.API()

# We get GeoJSON data by default
response = api.get('''
(
  node["checkin:covid19:luca-app"="optional"];
  way["checkin:covid19:luca-app"="optional"];
  rel["checkin:covid19:luca-app"="optional"];
);
out center;

''')

with open("data.geojson", "w") as fp:
    json.dump(response, fp)

print("data written to data.geojson")
