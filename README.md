# covid19-checkin-map

A map showing venues marked with COVID19-related check-in policies

This map is part of a project of a German data protection activist group and
also here as a note to self and for learning.


## 1) Fetch OpenStreetMap (OSM) data...

Overview: https://towardsdatascience.com/loading-data-from-openstreetmap-with-python-and-the-overpass-api-513882a27fd0

http://overpass-turbo.eu offers a request API we can use.
We could fetch the data with `curl`:

     # curl --globoff -o output.json \
           http://overpass-api.de/api/interpreter\?data\=\[out:json\]\;\(node\[\"checkin:covid19:luca-app\"=\"optional\"\]\;way\[\"checkin:covid19:luca-app\"=\"optional\"\]\;relation\[\"checkin:covid19:luca-app\"=\"optional\"\]\;\)\;out\;

To get optional check-ins. Alternative values for `checkin:covid19:luca-app` key are:

     * `mandatory`
     * `yes`
     * `no`

We could also use standard Python (with no external packages added). But as OSM
by default sends data in either XML or an own JSON-Format, we will deploy the
Python `overpass` package.

See the `get_geojson_data.py` script included.

The `overpass` package allows automatic generation of GeoJSON data.

We create a Python sandbox in order to avoid pestering the system Python with
otherwise not needed packages. In this sandbox (or virtualenv, how Pythonistas
call it), we install the neccessary package.

    $ python3 -m venv py3  # create sandbox in py3/
    (py3) $ sourcee py3/bin/activate  # activate sandbox
    (py3) $ pip install -U pip overpass   # install/upgrade packages pip and overpass.
    (py3) $ python get_geojson_data.py
    data written to data.geojson
    (py3) $ mv data.geojson site-content/data/optional.geojson

The file `data.geojson` contains all locations currently marked on OSM with special check-in policies.


## 2) An HTML-skeleton (html with leaflet)

For playing around we deploy a little local nginx-server. docker is well suited
for that task. Before we go on, we have to create the HTML code with leaflet
built-in.

We get leaflet from

     https://leafletjs.com/download.html

and do local hosting as we do not want our users to be tracked by CDNs.

The local directory `site-content/leaflet` gets the extracted `leaflet.zip`.

Make sure, there is an index.html in `site-content`.

With everythin in place we can start a docker container:

    $ docker run -it --rm -p 7070:80 --name nginx -v `pwd`/site-content:/usr/share/nginx/html nginx

starting `nginx` web-server on local port 7070. The process must be stopped by CTRL-C and doesn't leave too much garbage in memory.

Visit the map on http://localhost:7070

