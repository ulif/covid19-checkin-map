# leaflet directory

This directory should hold a copy of `leaflet`, a javascript library for
interactive maps.

You can get `leaflet` from

    https://leafletjs.com/download

Grab a copy and unzip it in this folder:

    $ wget http://cdn.leafletjs.com/leaflet/v1.7.1/leaflet.zip
    $ unzip leaflet.zip

The version number above might have increased since time of writing this.
